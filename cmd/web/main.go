package main

import (
	"course/pkg/apiserver"
	"flag"
	"github.com/BurntSushi/toml"
	"log"
)

var (
	configPath string
)

func init() {
	//val := "../../pkg/apiserver/configs/config.toml"
	flag.StringVar(&configPath, "config-path", "", "path to config file")
}

func main() {
	flag.Parse()

	config := apiserver.NewConfig()
	_, err := toml.Decode(configPath, config)
	if err != nil {
		log.Fatal(err)
	}
	server := apiserver.New(config)

	if err := server.Start(); err != nil {
		log.Fatal(err)
	}
}
