package db

import (
	"database/sql"
	_ "github.com/lib/pq"
)

type Store struct {
	config           *Config
	DB               *sql.DB
	courseRepository *CourseRepository
}

func New(config *Config) *Store {
	return &Store{
		config: config,
	}
}

func (s *Store) Open() error {

	db, err := sql.Open("postgres", s.config.DatabaseURL)
	if err != nil {
		return err
	}

	if err := db.Ping(); err != nil {
		return err
	}
	s.DB = db

	return nil
}

func (s *Store) Close() {
	err := s.DB.Close()
	if err != nil {
		return
	}
}

func (s *Store) Course() *CourseRepository {
	if s.courseRepository != nil {
		return s.courseRepository
	}

	s.courseRepository = &CourseRepository{
		Store: s,
	}

	return s.courseRepository
}
