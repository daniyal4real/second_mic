package db

import (
	"course/pkg/models"
	"database/sql"
	"fmt"
)

type CourseRepository struct {
	Store *Store
}

func (r *CourseRepository) CreateCourse(c *models.Course) (*models.Course, error) {
	if err := r.Store.DB.QueryRow("INSERT INTO course(title, details) VALUES ($1, $2) RETURNING id",
		c.Title,
		c.Details,
	).Scan(&c.ID); err != nil {
		return nil, err
	}
	return c, nil
}

func (r *CourseRepository) GetAllCourses() ([]*models.Course, error) {
	rows, err := r.Store.DB.Query("SELECT * FROM course")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var courses []*models.Course
	for rows.Next() {
		course := &models.Course{}
		if err := rows.Scan(&course.ID, &course.Title, &course.Details); err != nil {
			return nil, err
		}
		courses = append(courses, course)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return courses, nil
}

func (r *CourseRepository) GetCourseByID(id int) (*models.Course, error) {
	c := &models.Course{}

	err := r.Store.DB.QueryRow(
		"SELECT id, title, details FROM course WHERE id = $1",
		id,
	).Scan(
		&c.ID,
		&c.Title,
		&c.Details,
	)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, fmt.Errorf("Course with ID %d not found", id)
		} else {
			return nil, err
		}
	}

	return c, nil
}

func (r *CourseRepository) UpdateCourse(c *models.Course) (*models.Course, error) {
	if err := r.Store.DB.QueryRow("UPDATE course SET title = $1, details = $2 WHERE id = $3 RETURNING id, title, details",
		c.Title,
		c.Details,
		c.ID,
	).Scan(&c.ID, &c.Title, &c.Details); err != nil {
		return nil, err
	}
	return c, nil
}

func (r *CourseRepository) DeleteCourse(id int) error {
	_, err := r.Store.DB.Exec("DELETE FROM course WHERE id = $1", id)
	if err != nil {
		return err
	}
	return nil
}

func (r *CourseRepository) GetCoursesByStudentID(studentID int) ([]*models.Course, error) {
	courses := make([]*models.Course, 0)

	rows, err := r.Store.DB.Query(
		`SELECT c.id, c.title, c.details
			FROM course c
			INNER JOIN student_course sc ON c.id = sc.course_id
			WHERE sc.student_id = $1`,
		studentID,
	)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		c := &models.Course{}
		err := rows.Scan(&c.ID, &c.Title, &c.Details)
		if err != nil {
			return nil, err
		}
		courses = append(courses, c)
	}

	if err := rows.Err(); err != nil {
		return nil, err
	}

	return courses, nil
}
