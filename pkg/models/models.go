package models

type Course struct {
	ID      int    `json:"id"`
	Title   string `json:"title"`
	Details string `json:"details"`
}
