package apiserver

import "course/pkg/db"

type Config struct {
	BindAddress string `toml:"bind_address"`
	LogLevel    string `toml:"log_level"`
	Store       *db.Config
}

func NewConfig() *Config {
	return &Config{
		BindAddress: ":9090",
		LogLevel:    "debug",
		Store:       db.NewConfig(),
	}
}
