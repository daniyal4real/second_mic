package apiserver

import (
	"course/pkg/models"
	"encoding/json"
	"log"
	"net/http"
	"path"
	"strconv"
)

func (s *APIServer) Create(w http.ResponseWriter, r *http.Request) {
	course := &models.Course{}
	if err := Parse(r, course); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, err := w.Write([]byte("Error parsing request body"))
		if err != nil {
			return
		}
		return
	}

	newCourse, err := s.store.Course().CreateCourse(course)
	if err != nil {
		log.Printf("Error creating course: %v\n", err) // print the error message
		w.WriteHeader(http.StatusInternalServerError)
		_, err := w.Write([]byte("Error creating course"))
		if err != nil {
			return
		}
		return
	}

	res, _ := json.Marshal(newCourse)
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(res)
	if err != nil {
		return
	}
}

func (s *APIServer) GetCourses(w http.ResponseWriter, r *http.Request) {
	courses, err := s.store.Course().GetAllCourses()
	if err != nil {
		log.Fatal(err)
	}
	res, _ := json.Marshal(courses)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(res)
	if err != nil {
		return
	}
}

func (s *APIServer) GetCourseByID(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(path.Base(r.URL.Path))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, err := w.Write([]byte("Invalid course ID"))
		if err != nil {
			return
		}
		return
	}
	course, err := s.store.Course().GetCourseByID(id)
	if err != nil {
		if err.Error() == "Course not found" {
			w.WriteHeader(http.StatusNotFound)
			_, err := w.Write([]byte("Course not found"))
			if err != nil {
				return
			}
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		_, err := w.Write([]byte("Error getting course"))
		if err != nil {
			return
		}
		return
	}

	res, _ := json.Marshal(course)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(res)
	if err != nil {
		return
	}
}

func (s *APIServer) UpdateCourse(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(path.Base(r.URL.Path))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, err := w.Write([]byte("Invalid course ID"))
		if err != nil {
			return
		}
		return
	}

	updatedCourse := &models.Course{}
	if err := json.NewDecoder(r.Body).Decode(updatedCourse); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, err := w.Write([]byte("Error parsing request body"))
		if err != nil {
			return
		}
		return
	}

	updatedCourse.ID = id

	if updated, err := s.store.Course().UpdateCourse(updatedCourse); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, err := w.Write([]byte("Error updating course"))
		if err != nil {
			return
		}
		return
	} else {
		res, _ := json.Marshal(updated)
		w.WriteHeader(http.StatusOK)
		_, err := w.Write(res)
		if err != nil {
			return
		}
	}
}

func (s *APIServer) DeleteCourse(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(path.Base(r.URL.Path))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, err := w.Write([]byte("Invalid course ID"))
		if err != nil {
			return
		}
		return
	}

	if err := s.store.Course().DeleteCourse(id); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, err := w.Write([]byte("Error deleting course"))
		if err != nil {
			return
		}
		return
	}

	w.WriteHeader(http.StatusOK)
	_, err = w.Write([]byte("Course deleted successfully"))
	if err != nil {
		return
	}
}

func (s *APIServer) GetCoursesByStudentID(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.Atoi(path.Base(r.URL.Path))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, err := w.Write([]byte("Invalid student ID"))
		if err != nil {
			return
		}
		return
	}

	courses, err := s.store.Course().GetCoursesByStudentID(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, err := w.Write([]byte("Error getting courses"))
		if err != nil {
			return
		}
		return
	}

	res, _ := json.Marshal(courses)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(res)
	if err != nil {
		return
	}
}
