package apiserver

func (s *APIServer) configureRouter() {
	api := s.router.PathPrefix("/api").Subrouter()
	api.HandleFunc("/courses", s.Create).Methods("POST")
	api.HandleFunc("/courses", s.GetCourses).Methods("GET")
	api.HandleFunc("/courses/{id:[0-9]+}", s.GetCourseByID).Methods("GET")
	api.HandleFunc("/courses/{id:[0-9]+}", s.UpdateCourse).Methods("PUT")
	api.HandleFunc("/courses/{id:[0-9]+}", s.DeleteCourse).Methods("DELETE")
	api.HandleFunc("/students/{id:[0-9]+}/course", s.GetCoursesByStudentID).Methods("GET")
}
